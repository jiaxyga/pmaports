# Maintainer: Luca Weiss <luca@z3ntu.xyz>

_flavor="postmarketos-qcom-sm6350"
pkgname=linux-$_flavor
pkgver=5.17_rc5
pkgrel=0
pkgdesc="Mainline Kernel fork for SM6350 devices"
arch="aarch64"
_carch="arm64"
url="https://github.com/z3ntu/linux"
license="GPL-2.0-only"
options="!strip !check !tracedeps
	pmb:cross-native
	pmb:kconfigcheck-nftables
	"
makedepends="bison findutils flex installkernel openssl-dev perl"

_repo="linux"
_config="config-$_flavor.$arch"
_commit="723fdb477e17a148002a5d36ffad2fbf4a1841a3"

# Source
source="
	$_repo-$_commit.tar.gz::https://github.com/z3ntu/$_repo/archive/$_commit/$_repo-$_commit.tar.gz
	$_config
"
builddir="$srcdir/$_repo-$_commit"

prepare() {
	default_prepare
	cp "$srcdir/config-$_flavor.$arch" .config
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-$_flavor"
}

package() {
	mkdir -p "$pkgdir"/boot

	install -Dm644 "$builddir/arch/$_carch/boot/Image.gz" \
		"$pkgdir/boot/vmlinuz"

	make modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_PATH="$pkgdir"/boot/ \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_MOD_STRIP=1 \
		INSTALL_DTBS_PATH="$pkgdir"/usr/share/dtb
	rm -f "$pkgdir"/lib/modules/*/build "$pkgdir"/lib/modules/*/source

	install -D "$builddir"/include/config/kernel.release \
		"$pkgdir"/usr/share/kernel/$_flavor/kernel.release
}

sha512sums="
a1f471c85fb86fbf6d4c122d9190ce2e4f42b7a7ad2061d21ce0219fd58ff66e1c61d22e5aaf3ce87bfa4522487e8046131ace10babd8c101598ea8168de264d  linux-723fdb477e17a148002a5d36ffad2fbf4a1841a3.tar.gz
3e43cd19f2ef00dba5ee221ab13d32b2f50d35a7af5ec5675145521c826f6cde86496112212f0337d7b857f07c2743aacd8dd6f1dd6c83d2cb6647a04fbc4a30  config-postmarketos-qcom-sm6350.aarch64
"
